<h1 align="center">Instruccions</h1>

- [x] Los componentes no deben de salirse del div con id "parent" al arrastrarse

- [x] Cada componente debe de tener una imagen única, la cual con estilos propios, no debe salir del componente al que se le hace resize y deben de tener un fit diferente "cover", "contain", etc. **Esta se obtiene de un fetch a la api "https://jsonplaceholder.typicode.com/photos"**

- [x] Se debe de mantener la selección correctamente al hacer resize o drag, desde cualquiera de los 8 puntos (es decir, debe abarcar el componente mismo, no debe de estar abarcando cosas fuera de el).

- [x] Los componentes se deben de poder eliminar de la lista de componentes

- [x] La librería tiene la capacidad de mostrar las líneas guía de cada componente, debes mostrarlas cada que se haga drag del componente seleccionado. La documentación de la librería está aquí: https://daybrush.com/moveable/release/latest/doc/

- [x] También deberás documentar las funciones correctamente
