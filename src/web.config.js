/** Sample Api where the images will be requested */
const API = "https://jsonplaceholder.typicode.com/photos";
/** List of colors  */
const COLORS = ["red", "blue", "yellow", "green", "purple"];
/** Default position and size */
const POSITION_AND_SIZE = {
  top: 0,
  left: 0,
  width: 100,
  height: 100,
};

export { API, COLORS, POSITION_AND_SIZE };
